
import time
import pygame
import random
import os

class AudioController:

    currentMood = "Idle"
    newMood = ""
    audioMixer = pygame.mixer
    musicPlaying = False
    currentFile = './music/idle/idle02.mp3'

    def __init__(self):
        self.audioMixer.init()

    def playMusic(self):
        self.musicPlaying = True
        print("CURRENT FILE: "+self.currentFile)
        self.audioMixer.music.load(self.currentFile)
        self.audioMixer.music.play(loops=-1)
        while self.audioMixer.music.get_busy():
            if self.musicPlaying:
                time.sleep(5)
                self.checkCurrentMood()
                

    def stopMusic(self):
        print("STOPING MUSIC!")
        self.musicPlaying = False
        self.audioMixer.music.fadeout(1000)

    def changeFile(self, emotion):
        if emotion == 'Joy':
            files = self.getMusicFiles('joy')
            newFile = './music/joy/'+ random.choice(files)
        elif emotion == 'Sadness':
            files = self.getMusicFiles('sadness')
            newFile = './music/sadness/'+ random.choice(files)
        elif emotion == 'Angry':
            files = self.getMusicFiles('angry')
            newFile = './music/angry/'+ random.choice(files)
        elif emotion == 'Suspense':
            files = self.getMusicFiles('suspense')
            newFile = './music/suspense/'+ random.choice(files)
        elif emotion == 'Suprise':
            files = self.getMusicFiles('suprise')
            newFile = './music/suprise/'+ random.choice(files)
        elif emotion == 'Idle':
            files = self.getMusicFiles('idle')
            newFile = './music/idle/'+ random.choice(files)
        self.currentFile = newFile

    def combineMoodValues(self, emotion):
        if emotion == 'Positive' or emotion == 'Joy' or emotion == 'Trust':
            return 'Joy'
        elif emotion == 'Negative' or emotion == 'Sadness':
            return 'Sadness'
        elif emotion == 'Anger' or emotion == 'Disgust':
            return 'Angry'
        elif emotion == 'Anticipation' or emotion == 'Fear':
            return 'Suspense'
        elif emotion == 'Suprise':
            return 'Suprise'
        elif emotion == 'Positive':
            return 'Idle'

    def checkCurrentMood(self):
        if self.currentMood != self.newMood and self.newMood != "":
            self.currentMood = self.newMood
            self.audioMixer.music.fadeout(1000)
            self.changeFile(self.currentMood)
            self.playMusic()

    def getMusicFiles(self, directory):
        allFiles = os.listdir('./music/'+ directory)
        files = []
        for file in allFiles:
            if not file.startswith('.'):
                files.append(file)
        return files

"""
username = "puffelmuffel?si=QW7m5lg1SgCv2d9UImu8dg"
scope = 'user-library-read'

#UserID: puffelmuffel?si=QW7m5lg1SgCv2d9UImu8dg

try:
    token = util.prompt_for_user_token(username, scope)
except:
    os.remove(f".cache-{username}")
    token = util.prompt_for_user_token(username)

spotifyObject = spotipy.Spotify(auth=token)

results = spotifyObject.search(q='happy', limit=20)
"""