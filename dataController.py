import pandas as pd

class Calculator:

    libraryFile = 'emotion_lexicon.xlsx'

    df = pd.read_excel(libraryFile, sheet_name='Sheet1')
    # remove words from list without emotional value
    df = df[df.EmotionalValue != 0]

    def __init__(self, language):
        self.df.set_index(language, inplace=True)
        self.df.index = self.df.index.str.lower()
        
    def splitRecordedString(self, recordedStringAsList):
        return ' '.join(recordedStringAsList).split()

    def checkEmotionalValue(self, newWords):
        emotionalValue = [0,0,0,0,0,0,0,0,0,0]
        for newWord in newWords:
            # check if word is represented in database
            if self.df.index.contains(newWord):
                allMatches = self.df.loc[newWord]
                chosenMatch = ""
                # check if one or more matches
                if len(allMatches) == 15:
                    chosenMatch = allMatches
                else :
                    # get match with highest emotional value
                    chosenMatch = allMatches[allMatches['EmotionalValue'] == allMatches['EmotionalValue'].max()]
                    # if more then one matches have the same max value, get first
                    if len(chosenMatch) != 15:
                        chosenMatch = chosenMatch.iloc[0]
                    else:
                        chosenMatch = chosenMatch.loc[newWord]
                
                if chosenMatch['Positive'] == 1:
                    emotionalValue[0] += 1
                if chosenMatch['Negative'] == 1:
                    emotionalValue[1] += 1
                if chosenMatch['Anger'] == 1:
                    emotionalValue[2] += 1
                if chosenMatch['Anticipation'] == 1:
                    emotionalValue[3] += 1
                if chosenMatch['Disgust'] == 1:
                    emotionalValue[4] += 1
                if chosenMatch['Fear'] == 1:
                    emotionalValue[5] += 1
                if chosenMatch['Joy'] == 1:
                    emotionalValue[6] += 1
                if chosenMatch['Sadness'] == 1:
                    emotionalValue[7] += 1
                if chosenMatch['Surprise'] == 1:
                    emotionalValue[8] += 1
                if chosenMatch['Trust'] == 1:
                    emotionalValue[9] += 1
            else:
                print(newWord + " has no emotional value!")

        if emotionalValue != [0,0,0,0,0,0,0,0,0,0]:
            return emotionalValue 
        else:
            return []

    def emotion(self, index):
            switcher={
                    0: 'Positive',
                    1: 'Negative',
                    2: 'Anger',
                    3: 'Anticipation',
                    4: 'Disgust',
                    5: 'Fear',
                    6: 'Joy',
                    7: 'Sadness',
                    8: 'Suprise',
                    9: 'Trust'
                }
            return switcher.get(index,"Invalid day of week")

#calc = Calculator('German')
#calc.checkEmotionalValue(["sie", "durch"])