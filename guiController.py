from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.togglebutton import ToggleButton
from kivy.clock import Clock
import threadController

threader = threadController.ThreadController()

class Moodsician(Widget):
    toggleButton = ToggleButton()

    def toggleListening(self, startListening):
        if startListening == True:
            print('listening...')
            threader.start()
        else:
            threader.stop()

    def test(self):
        threader.test()

class MoodsicianApp(App):
    def build(self):
        self.load_kv('moodsician.kv')
  
        return Moodsician()

if __name__ == '__main__':   
    MoodsicianApp().run()