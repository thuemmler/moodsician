import speech_recognition as sr  # import the library
from pocketsphinx import LiveSpeech
from ibm_watson import SpeechToTextV1
from ibm_watson.websocket import RecognizeCallback, AudioSource
from os.path import join, dirname
import json

class MyRecognizeCallback(RecognizeCallback):

    def __init__(self):
        RecognizeCallback.__init__(self)

    def on_data(self, data):
        print(json.dumps(data, indent=2))

    def on_error(self, error):
        print('Error received: {}'.format(error))

    def on_inactivity_timeout(self, error):
        print('Inactivity timeout: {}'.format(error))

myRecognizeCallback = MyRecognizeCallback()

class RecordController():

    recording = False
    currentData = ""

    def __init__(self):
        self.recording = False

    def recordAudio(self):
        # Record Audio
        self.recording = True
        r = sr.Recognizer()
        data = ""
        while self.recording:
            with sr.Microphone() as source:
                print("Say something!")
                #r.adjust_for_ambient_noise(source)
                #r.phrase_threshold = 0.8
                #recordedAudio = r.listen(source)
                recordedAudio = r.record(source, duration = 3.5)
                data = self.callbackRecognize(recordedAudio)
            return data
    
    def callbackRecognize(self, audio):
        speech_to_text = SpeechToTextV1(
            iam_apikey='CXTZrS0LVriUn6CGQqtZvT8LmdYQAGzAhIYlgfgsxImm',
            url='https://stream-fra.watsonplatform.net/speech-to-text/api')
        result = ""
        try:
            # de-DE // en-US // en-GB // es-ES // fr-FR
            speech_recognition_results = speech_to_text.recognize(audio = audio.get_wav_data(), content_type='audio/wav', model= 'en-US_BroadbandModel').get_result()
            try:
                returnedData1 = speech_recognition_results.get("results")[0]
                returnedData2 = returnedData1.get("alternatives")[0]
                result = returnedData2.get("transcript")
            except:
                print("results empty!")
            
        except sr.UnknownValueError:
            print("Speech Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Speech Recognition service; {0}".format(e))
        except sr.WaitTimeoutError as e:
            print("Timed Out")

        return result

    def stopRecording(self):
        self.recording = False

    def recordLiveAudio(self):
        for phrase in LiveSpeech(): print(phrase)