from threading import Thread,Event
import time
import queue
import recordController
import audioController
import dataController

class ThreadController(object):

    q = queue.Queue()
    audioPlayer = audioController.AudioController()
    recorder = recordController.RecordController()
    calculator = dataController.Calculator('English')

    currentRecordedData = ['']
    currentRecordedWords = []
    currentMood = [0,0,0,0,0,0,0,0,0,0]
    previousMood = ''
    newMood = ''

    newRecordedData = False
    switchingSong = False

    def __init__(self):
        self.recordThread = None
        self.audioThread = None
        self.calculatorThread = None
        self.stop_threads = False

    def record(self):
        while True:
            self.currentRecordedData[0] = self.recorder.recordAudio()
            print('you said: ' + self.currentRecordedData[0])
            self.currentRecordedWords = self.calculator.splitRecordedString(self.currentRecordedData)
            self.newRecordedData = True
            if self.stop_threads:
                print('stopped listening!')
                break

    def playAudio(self):
        while True:
            self.audioPlayer.playMusic()
            if self.stop_threads:
                print('stopped playing!')
                break

    def startCalculation(self):
        while True:
            time.sleep(3)
            self.calculateNewMood()
            if self.stop_threads:
                print('stopped checking!')
                break

    # check current mood and switch song if mood changed
    def checkCurrentMood(self):
        combinedPreviousMood = self.audioPlayer.combineMoodValues(self.previousMood)
        combinedNewMood = self.audioPlayer.combineMoodValues(self.newMood)
        if combinedPreviousMood != combinedNewMood:
            print('Should Change to: '+combinedNewMood)
            self.audioPlayer.newMood = combinedNewMood
        self.newRecordedData = False
        

    # adds new values to the current mood
    def calculateNewMood(self):
        if self.newRecordedData:
            self.previousMood = self.calculator.emotion(self.currentMood.index(max(self.currentMood)))

            emotionalValues = self.calculator.checkEmotionalValue(self.currentRecordedWords)
            if emotionalValues != []:
                index = 0
                for value in emotionalValues:
                    if index >= 2:
                        if value == 1:
                            self.currentMood[index] += value
                        else:
                            if self.currentMood[index] != 0:
                                self.currentMood[index] -= 1
                    index += 1
                print("current MOOD: "+ str(self.currentMood))

            self.newMood = self.calculator.emotion(self.currentMood.index(max(self.currentMood)))

            self.checkCurrentMood()

    #Test Method
    def test(self):
        print(self.currentMood)

    def start(self):
            self.stop_threads = False
            self.recordThread = Thread(target = self.record)
            self.recordThread.daemon = True
            self.recordThread.start()
            self.audioThread = Thread(target = self.playAudio)
            self.audioThread.daemon = True
            self.audioThread.start()
            self.calculatorThread = Thread(target = self.startCalculation)
            self.calculatorThread.daemon = True
            self.calculatorThread.start()

    def stop(self):
        self.stop_threads = True
        self.audioPlayer.musicPlaying = False
        self.audioPlayer.stopMusic()